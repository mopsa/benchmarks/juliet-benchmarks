Benchmarks of Mopsa on Juliet Test Suite
========================================

This repository contains the scripts to run Mopsa on Juliet Test Suite for C/C++.


Quick Start
-----------
Before starting the analysis, first download the sources of Juliet by issuing:
```shell
$ make prepare
```

To run the analysis of all (supported) test cases:
```shell
$ make
```
It is possible to run the tests in parallel by issuing `make -j<N>`.

Each test case corresponds to a single CWE and contains a good and a bad function.
The good function is safe while the bad function contains a single bug instance of the targeted CWE.
For each test, Mopsa analyzes the good and the bad functions separately.
Four outcomes are possible:
1. If no alarm is found in the good function, and one alarm is found in the bad function (that corresponds to the CWE), the analysis is *successful*.
3. If the analysis reports alarms in the good test or more than one alarm in the bad case, the analysis is *imprecise*.
2. If no alarm corresponding to the CWE is found in the bad function, the analysis is *unsound*.
4. Finally, if the program contains features that can't be analyzed by Mopsa, the test is *unsupported*.


Note that it is possible to run an individual test:
```shell
$ make CWE476_NULL_Pointer_Dereference/CWE476_NULL_Pointer_Dereference__int_01
```
or all tests of a single CWE:
```shell
$ make CWE476_NULL_Pointer_Dereference
```

Also, it is possible to see the analysis commands that are executed, by definig the environment variable `VERBOSE`:

``` shell
$ VERBOSE=1 make CWE476_NULL_Pointer_Dereference/CWE476_NULL_Pointer_Dereference__int_01
```

Results
-------

The summary of the analysis can be obtained by running:

``` shellsession
$ make stats
+---------+--------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+-------------+
|   CWE   |             Title              |      Time       |    Total    |   Success   |  Imprecise  |   Unsound   |   Failure   | Unsupported |
+---------+--------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+-------------+
| CWE121  |  Stack-based Buffer Overflow   |   01:04:51.31   |    2508     |     82%     |     17%     |     0%      |     0%      |     0%      |
| CWE122  |   Heap-based Buffer Overflow   |   00:40:50.34   |    1556     |     81%     |     18%     |     0%      |     0%      |     0%      |
| CWE124  |       Buffer Underwrite        |   00:20:14.70   |     758     |     77%     |     22%     |     0%      |     0%      |     0%      |
| CWE126  |        Buffer Over-read        |   00:16:08.21   |     600     |     88%     |     11%     |     0%      |     0%      |     0%      |
| CWE127  |       Buffer Under-read        |   00:20:08.91   |     758     |     78%     |     21%     |     0%      |     0%      |     0%      |
| CWE190  |        Integer Overflow        |   01:30:46.59   |    3420     |     74%     |     25%     |     0%      |     0%      |     0%      |
| CWE191  |       Integer Underflow        |   01:07:44.16   |    2622     |     78%     |     21%     |     0%      |     0%      |     0%      |
| CWE369  |         Divide By Zero         |   00:14:16.93   |     497     |     70%     |     29%     |     0%      |     0%      |     0%      |
| CWE415  |          Double Free           |   00:04:44.59   |     190     |    100%     |     0%      |     0%      |     0%      |     0%      |
| CWE416  |         Use After Free         |   00:02:57.72   |     118     |    100%     |     0%      |     0%      |     0%      |     0%      |
| CWE469  | Use of Pointer Subtraction ..  |   00:00:27.08   |     18      |    100%     |     0%      |     0%      |     0%      |     0%      |
| CWE476  |    NULL Pointer Dereference    |   00:05:18.69   |     216     |    100%     |     0%      |     0%      |     0%      |     0%      |
+---------+--------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+-------------+
|                  Total                   |   05:48:29.23   |    13261    |     79%     |     20%     |     0%      |     0%      |     0%      |
+------------------------------------------+-----------------+-------------+-------------+-------------+-------------+-------------+-------------+
```

The results of the analysis are stored in JSON files located in the `results` directory.

Coverage
--------
Not all tests are considered.
Mopsa targets only CWEs related to runtime errors representing undefined behaviors, as defined by the C Standard.
The list of supported CWEs is (see `support/cwe.txt`):

| ID     | Title                                              |
| ------ | -------------------------------------------------- |
| CWE121 | Stack-based Buffer Overflow                        |
| CWE122 | Heap-based Buffer Overflow                         |
| CWE124 | Buffer Underwrite                                  |
| CWE126 | Buffer Over-read                                   |
| CWE127 | Buffer Under-read                                  |
| CWE190 | Integer Overflow                                   |
| CWE191 | Integer Underflow                                  |
| CWE369 | Divide By Zero                                     |
| CWE415 | Double Free                                        |
| CWE416 | Use After Free                                     |
| CWE469 | Use of Pointer Subtraction to Determine Size       |
| CWE476 | NULL Pointer Dereference                           |


All C files present in these directories are analyzed, except some particular cases which are not related to runtime errors.
The excluded tests can found in `support/excluded.txt`.
