#!/usr/bin/python3
##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################

# Script to generate a configuration for GitLab CI to run Juliet benchmarks

import argparse
import os
import yaml
from common import *

########################
# Command line options #
########################

parser = argparse.ArgumentParser(description='Generator of GitLab CI configuration to run MOPSA on Juliet tests.')
parser.add_argument('--juliet',
                    help="path to Juliet Test Suite")
args = parser.parse_args()


##################
# Juliet Objects #
##################

# Juliet CWE with a list of test cases
class CWE:
    def __init__(self, dir_name):
        self.dir_name = dir_name
        self.code = int(re.search('CWE(\d\d\d).*', dir_name).group(1))
        self.name = cwe_name[self.code]


# Juliet object
class Juliet:
    def __init__(self, path):
        self.cwe_list = []
        tests_path = "%s/C/testcases"%path
        for f in os.listdir(tests_path):
            fp = os.path.join(tests_path,f)
            if os.path.isdir(fp):
                self.cwe_list.append(CWE(f.strip()))
        self.cwe_list = sorted(self.cwe_list, key = lambda cwe: cwe.code)
        self.path = path


#####################
# GitLab CI Objects #
#####################


# Build stage
class BuildStage:
    def __init__(self, juliet):
        self.body = {
            "mopsa": {
                "stage": "Build",
                "script": [
                    "git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/mopsa/mopsa-analyzer.git",
                    "cd mopsa-analyzer",
                    "eval `opam config env`",
                    "./configure",
                    "make -j"
                ],
                "artifacts": {
                    "paths": [
                        "./mopsa-analyzer/bin/mopsa",
                        "./mopsa-analyzer/bin/mopsa-c",
                        "./mopsa-analyzer/bin/mopsa.bin",
                        "./mopsa-analyzer/bin/mopsa-diff",
                        "./mopsa-analyzer/analyzer/_build/mopsa.native",
                        "./mopsa-analyzer/_build/default/analyzer/mopsa.exe",
                        "./mopsa-analyzer/share",
                        "./mopsa-analyzer/tools/mopsa-diff/mopsa-diff",
                    ],
                    "expire_in": "4h"
                }
            }
        }

class CWEJob:
    def __init__(self, cwe, juliet):
        self.body = {
            "%s"%cwe.name: {
                "variables": {
                    "CWE": cwe.dir_name,
                    "JOB": cwe.name
                },
                "extends": ".test",
                "artifacts": {
                    "paths": [
                        "results/summary/%s"%(cwe.dir_name),
                        "results/details/%s"%(cwe.dir_name)
                    ],
                    "expire_in": "4 weeks"
                }
            }
        }



# CI object
class CI:
    def __init__(self, juliet):
        self.body = {
            "variables": {
                "DOCKER_DRIVER": "overlay2",
                "GIT_STRATEGY": "fetch",
                "PARAMS": "MOPSA=./mopsa-analyzer/bin/mopsa-c JULIET=/home/mopsa/juliet"
            },
            "image": "mopsa-juliet",
            "stages": ["Build", "Tests"],
        }
        self.body.update(BuildStage(juliet).body)
        self.body.update({
            ".test": {
                "stage": "Tests",
                "script": [
                    "make $CWE $PARAMS",
                    "./support/get-artifact.py juliet-benchmarks \"$JOB\" --token ${API_TOKEN} --group mopsa%2Fbenchmarks || true",
                    "if [ -f artifacts.zip ]; then unzip artifacts.zip -d oracle; ./mopsa-analyzer/bin/mopsa-diff --regression oracle/results/details/$CWE results/details/$CWE; fi"
                ]
            }
        })
        for cwe in juliet.cwe_list:
            self.body.update(CWEJob(cwe,juliet).body)


# Entry point

juliet = Juliet(args.juliet)
ci = CI(juliet)

print(yaml.dump(ci.body, default_flow_style=False, sort_keys=False))
