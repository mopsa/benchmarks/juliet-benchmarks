##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################


import os
import re


# Table of bindings between CWE codes and MOPSA alarms
cwe_to_mopsa_alarms = {
    121: ['Invalid memory access'],
    122: ['Invalid memory access'],
    124: ['Invalid memory access'],
    126: ['Invalid memory access'],
    127: ['Invalid memory access'],
    190: ['Integer overflow'],
    191: ['Integer overflow'],
    369: ['Division by zero'],
    415: ['Double free'],
    416: ['Invalid memory access'],
    469: ['Invalid pointer subtraction'],
    476: ['Invalid memory access'],
}


# Get the name of a CWE testcase
def get_cwe_testcase(cwe):
    base = os.path.basename(cwe)
    return os.path.splitext(base)[0]

# Get the code of a CWE
def get_cwe_code(cwe):
    name = get_cwe_testcase(cwe)
    m = re.search('CWE(\d\d\d).*', name)
    return int(m.group(1))

# Table of CWE names
cwe_name = {
    121: 'Stack-based Buffer Overflow',
    122: 'Heap-based Buffer Overflow',
    124: 'Buffer Underwrite',
    126: 'Buffer Over-read',
    127: 'Buffer Under-read',
    190: 'Integer Overflow',
    191: 'Integer Underflow',
    369: 'Divide By Zero',
    415: 'Double Free',
    416: 'Use After Free',
    469: 'Use of Pointer Subtraction to Determine Size',
    476: 'NULL Pointer Dereference',
}


# Table of status code
status_code = {
    "Success": 0,
    "Failure": 1,
    "Unsound": 2,
    "Imprecise": 0,
    "Unsupported": 4
}
