#!/usr/bin/python3
##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################


# Wrapper script to analyze a single Juliet CWE test with MOPSA

import argparse
import os
import subprocess
import json
import re
from common import *


# Get the command line arguments
def get_args():
    parser = argparse.ArgumentParser(description='Juliet CWE wrapper for MOPSA.')
    parser.add_argument('cwe',
                        help="Path to the CWE test to analyze")
    parser.add_argument('--mopsa',
                        help="Path to mopsa-c binary",
                        default="mopsa-c")
    parser.add_argument('--config',
                        help="Abstraction configuration",
                        default="c/cell-string-itv-zero.json")
    parser.add_argument('--summary-output-dir',
                        help="Path to output directory of the summary JSON file",
                        dest='summary')
    parser.add_argument('--details-output-dir',
                        help="Path to output directory of the detailed JSON files",
                        dest='details')
    parser.add_argument('--relax',
                        help="Fail only when a failure or a false negative is detected",
                        action='store_true')
    parser.add_argument('--verbose',
                        help="Display the issued mopsa-c commands",
                        action='store_true')
    return parser.parse_args()


# Execute a command in the shell
def exec_shell(cmd):
    return subprocess.check_output(cmd, shell=True).decode('utf-8')


# Get the list of files to analyze
def get_cwe_files(cwe):
    dirname = os.path.dirname(cwe)
    test = get_cwe_testcase(cwe)
    r = exec_shell("find %s/%s*.c"%(dirname,test))
    return r.splitlines()


# Get the root directory of Juliet
def get_juliet_root(cwe):
    path = os.path.dirname(cwe)
    while True:
        if os.path.isfile(path + "/testcasesupport/io.c"):
            return path
        path = os.path.normpath(path + "/..")


# Get MOPSA flags common between the analysis of good and bad cases
def get_common_mopsa_flags(cwe,config):
    files = get_cwe_files(cwe)
    juliet = get_juliet_root(cwe)
    return "-c-check-unsigned-arithmetic-overflow=true -format=json -silent -no-warning -config=%s -I=%s/testcasesupport %s/testcasesupport/io.c %s"%(config,juliet,juliet," ".join(files))


# Get MOPSA flags for the good case
def get_mopsa_flags(cwe,config,case):
    common = get_common_mopsa_flags(cwe,config)
    test = get_cwe_testcase(cwe)
    return common + " -c-entry=" + test + "_" + case


# Run MOPSA on the CWE case
def analyze(mopsa, cwe,config,case):
    flags = get_mopsa_flags(cwe,config,case)
    cmd = mopsa + " " + flags
    if args.verbose: print(cmd)
    r = exec_shell(cmd)
    return json.loads(r)

def access_alarms(json):
    if "mopsa_version" not in json or json["mopsa_version"] == "1.0~pre1":
        return json["alarms"]
    else:
        return [x for x in json["checks"] if x['kind'] in ["error", "warning"]]

def is_success(good,bad):
    return good['success'] and bad['success']

def is_supported(good,bad):
    return len(good['assumptions']) == len(bad['assumptions']) == 0

def is_sound(bad):
    return len(access_alarms(bad)) >= 1

def is_precise(good, bad, cwe):
    code = get_cwe_code(cwe)
    titles = cwe_to_mopsa_alarms[code]
    return len(access_alarms(good)) == 0 and len(access_alarms(bad)) == 1 and access_alarms(bad)[0]['title'] in titles

def get_status(good,bad,cwe):
    if not is_success(good, bad):
        return "Failure"
    if not is_supported(good, bad):
        return "Unsupported"
    if not is_sound(bad):
        return "Unsound"
    if not is_precise(good, bad, args.cwe):
        return "Imprecise"
    return "Success"


# Entry point
args = get_args()
good = analyze(args.mopsa, args.cwe, args.config, "good")
bad = analyze(args.mopsa, args.cwe, args.config, "bad")
status = get_status(good,bad,args.cwe)
code = get_cwe_code(args.cwe)
name = cwe_name[code]
test = get_cwe_testcase(args.cwe)

if (args.summary is not None):
    with open(args.summary + '/' + test + '.json', 'w') as f:
        json.dump(
            { "cwe-code": code,
              "cwe-name": name,
              "cwe-testcase": test,
              "status": status,
              "time": good["time"] + bad["time"] },
            f
        )
if (args.details is not None):
    with open(args.details + '/' + test + '.good.json', 'w') as f:
        json.dump(good,f)

    with open(args.details + '/' + test + '.bad.json', 'w') as f:
        json.dump(bad,f)


if status == "Success":
    print('\033[92m' + "✔"  + '\033[0m' + " %s successful."%test)

if status == "Unsound":
    print('\033[95m' + "✗"  + '\033[0m' + " %s unsound."%test)

if status == "Imprecise":
    print('\033[93m' + "⚠"  + '\033[0m' + " %s imprecise."%test)

if status == "Failure":
    print('\033[91m' + "◆"  + '\033[0m' + " %s failed."%test)

if status == "Unsupported":
    print('\033[96m' + "∗"  + '\033[0m' + " %s unsupported."%test)



if args.relax: exit(0)
else: exit(status_code[status])
